# spriteunu

## Fork
This is a fork from [mapbox spritezero](https://github.com/mapbox/spritezero). For project evoluttion, please reffer original README file under [README.old.md](README.old.md) and original [CHANGELOG.md](CHANGELOG.old.md).

# Motivation

Looks like Mapbox doesn't maintain anymore this tool, but we need for our internal workflows at Lightcyphers. For us is easier to maintain a fork without the need to negotiate PR-s at Mapbox. 